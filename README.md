<h1>Test App for DevOps-workshop</h1>

Dockerized Python application on Django with containerized PostgreSQL database.

<h3>Requirements</h3>

____



- install Docker 24.0.6
- check Docker version
```shell
      docker -v
```
<h3>Containerize</h3>

____
- Go to the root folder of the project
```shell
      cd /d <path>
```
- Сontainerize the application using the command below
```shell
      docker-compose up
```
- The application will be available via this link:
[http://localhost:8000](http://localhost:8000)

<h3>Control</h3>

____
- If you need to stop
```shell
      docker-compose stop
```
- If you need to start
```shell
      docker-compose start
```
- If you need to restart
```shell
      docker-compose restart
```