#Базовый образ Python
FROM python:3.8

#Переменные окружения
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

#Рабочая директория
WORKDIR /code

#Установка зависимостей для Django
COPY requirements.txt /code/
RUN pip install -r requirements.txt

#Копирование файлов проекта в контейнер
COPY . /code/